#include "NixieEEPROM.hpp"
#include "NixieHtml.hpp"
#include "NixieShift.hpp"
#include "NixieTime.hpp"
#include <Arduino.h>
#include <EEPROM.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <JC_Button.h>
#include <NTPClient.h>
#include <Ticker.h>
#include <WiFiUdp.h>

// PINS
#define SR_LATCH D2
#define SR_CLOCK D1
#define SR_DATA D3
#define BTN_INITIALISE D4

#define BUTTON_LONG_PRESS_DURATION_MS 3 * 1000 // 3 secodns
#define NTP_UPDATE_INTERVAL_MS 15 * 60 * 1000  // 15 minutes
#define NTP_UTC_TIMEZONE_OFFSET 1 * 60 * 60    // 1 hour
#define MAINTENANCE_INTERVAL_S 24 * 60 * 60    // 24 hours
#define MAINTENANCE_CYCLE_INTERVAL_MS 250
#define MAINTENANCE_NUMBER_OF_CYCLES 5
#define WIFI_WAIT_FOR_CONNECTION_MAX_TICK 30

#define SERIAL_DEBUG
// #define CLEAR_EEPROM

// ========== WiFi Setup ==========
ESP8266WebServer webServer(80);
String ssid;
String password;
// state to keep track how many ticks we have been waiting
// for a successful wifi connection
uint8_t wifiWaitForConnectionTick = 0;
// if true, we will write `ssid` and `password` into EEPROM
bool shouldPersistWiFiCredentials = false;

// ========== Nixie Setup ==========
NixieShift nixie(SR_LATCH, SR_CLOCK, SR_DATA);
// indicate if Nixie should be displaying anything
bool shouldShowNixie = true;

// ========== NTP Setup ==========
WiFiUDP ntpUDP;
// create ntp client, note that we will need to set timezone offset manually
// good luck with BST :-)
NTPClient ntpClient(ntpUDP, "ntp2d.mcc.ac.uk", NTP_UTC_TIMEZONE_OFFSET);
// state to keep track the last ntp update call
unsigned long lastNtpUpdate = 0, tempNow = 0;
// the seconds ticker
uint8_t seconds = 255;
// the stored time we got from last NTP
uint8_t storedHours, storedMinutes, storedSeconds = 255;
// temp holder used for NTP update
uint8_t tempHours, tempMinutes, tempSeconds = 255;
// the time that are currently displayed in Nixie
uint8_t hour1, hour2, minute1, minute2 = 255;

// Button(pin, debounceTime, puEnable, invert);
Button initialisationButton(BTN_INITIALISE, 25, true, true);

// ========== Maintenance Setup ==========
Ticker maintenanceTicker;
// state to keep track the number of maintenance cycles
// we have been through
uint8_t maintenanceCounter = 0;
// state to inform the FSM if it should go into Maintenance mode
bool shouldMaintainNow = false;

enum FSM_STATES {
  NOT_SET,         // first powers on
  INITIALISATION,  // requires WiFi initialisation
  CONNECTING_WIFI, // is attempting to connect to wifi
  WAIT_FOR_NTP,    // is online and waiting for ntp response
  TICKTOCK,        // has everything it needs to operate
  MAINTENANCE      // cycling through the display (reduce Nixie poisoning)
};
FSM_STATES state = NOT_SET;

#ifdef SERIAL_DEBUG
String currentState() {
  String s;
  switch (state) {
  case NOT_SET:
    s = "NOT_SET";
    break;
  case INITIALISATION:
    s = "INITIALISATION";
    break;
  case CONNECTING_WIFI:
    s = "CONNECTING_WIFI";
    break;
  case WAIT_FOR_NTP:
    s = "WAIT_FOR_NTP";
    break;
  case TICKTOCK:
    s = "TICKTOCK";
    break;
  case MAINTENANCE:
    s = "MAINTENANCE";
    break;
  }
  return s;
}
#endif

void transitToWaitForNTP() {
  delay(500);
#ifdef SERIAL_DEBUG
  Serial.println("FSM: " + currentState() + " -> " + "WAIT_FOR_NTP");
#endif
  ntpClient.begin();
  state = WAIT_FOR_NTP;
}

void transitToConnectingWifi() {
  delay(500);
#ifdef SERIAL_DEBUG
  Serial.println("FSM: " + currentState() + " -> " + "CONNECTING_WIFI");
#endif
  wifiWaitForConnectionTick = 0;
  webServer.close();
  WiFi.begin(ssid, password);
  state = CONNECTING_WIFI;
}

void transitToTickTock() {
  delay(500);
  shouldMaintainNow = false;
#ifdef SERIAL_DEBUG
  Serial.println("FSM: " + currentState() + " -> " + "TICKTOCK");
#endif
  state = TICKTOCK;
}

void transitToInitialisation() {
  delay(500);
#ifdef SERIAL_DEBUG
  Serial.println("FSM: " + currentState() + " -> INITIALISATION");
#endif
  WiFi.softAP("NIXIE 9000", "password");
  webServer.on("/", HTTP_GET,
               []() { webServer.send(200, "text/html", homePage); });
  webServer.on("/thankyou", HTTP_GET,
               []() { webServer.send(200, "text/html", thankYouPage); });
  webServer.on("/wifi", HTTP_POST, []() {
    if (webServer.hasArg("SSID") && webServer.hasArg("PASSWORD")) {
      ssid = webServer.arg("SSID");
      password = webServer.arg("PASSWORD");
      shouldPersistWiFiCredentials = true;
      webServer.sendHeader("Location", "/thankyou", true);
#ifdef SERIAL_DEBUG
      Serial.println("INPUT SSID: " + ssid);
      Serial.println("INPUT PASSWORD: " + password);
#endif
      transitToConnectingWifi();
    } else {
      webServer.sendHeader("Location", "/thankyou", true);
    }
    webServer.send(302, "text/plain", "");
  });
  webServer.begin();
  state = INITIALISATION;
}

void transitToMaintenance() {
  delay(500);
#ifdef SERIAL_DEBUG
  Serial.println("FSM: " + currentState() + " -> " + "MAINTENANCE");
#endif
  shouldShowNixie = true;
  maintenanceCounter = 0;
  state = MAINTENANCE;
}

void flagMaintainNow() { shouldMaintainNow = true; }

void setup() {
#ifdef SERIAL_DEBUG
  Serial.begin(74880);
#endif
  nixie.display(0, 0, 0, 0);
  delay(1000); // wait for WiFi to catch up
  initialisationButton.begin();
  ntpClient.setUpdateInterval(NTP_UPDATE_INTERVAL_MS);
  maintenanceTicker.attach(MAINTENANCE_INTERVAL_S, flagMaintainNow);
  // so far 32 + 32 + 1 = 65 bytes are being used
  EEPROM.begin(512);
  char tempSsid[32], tempPassword[32];
  if (readWiFiCredentials(tempSsid, tempPassword)) {
    ssid = String(tempSsid);
    password = String(tempPassword);
#ifdef SERIAL_DEBUG
    Serial.println("EEPROM SSID: " + ssid);
    Serial.println("EEPROM PASSWORD: " + password);
#endif
    transitToConnectingWifi();
  } else {
#ifdef SERIAL_DEBUG
    Serial.println("EEPROM WIFI CREDENTIALS NOT FOUND");
#endif
    transitToInitialisation();
  }
}

void loop() {
  initialisationButton.read();

  if (initialisationButton.wasReleased()) {
#ifdef CLEAR_EEPROM
    writeHasValue(false);
    Serial.println("Writing HAS_VALUE = FALSE to EEPROM");
#else
    if (state == INITIALISATION) {
      transitToConnectingWifi();
    } else {
      transitToInitialisation();
    }
#endif
  }

  // BIG FAT STATE MACHINE 9000!!!
  switch (state) {
  case NOT_SET:
    break;
  case INITIALISATION:
    nixie.display(1, 1, 1, 1);
    webServer.handleClient();
    delay(100);
    break;
  case CONNECTING_WIFI:
    nixie.display(2, 2, 2, 2);
    if (wifiWaitForConnectionTick++ > WIFI_WAIT_FOR_CONNECTION_MAX_TICK) {
#ifdef SERIAL_DEBUG
      Serial.println("Not able to connect to WIFI");
#endif
      transitToInitialisation();
    } else if (WiFi.status() == WL_CONNECTED) {
      // only when it's connected that we will persist the wifi credentials
      if (shouldPersistWiFiCredentials) {
        bool write = writeWiFiCredentials(ssid, password);
#ifdef SERIAL_DEBUG
        Serial.println("Write to EEPROM successful? " + write);
#endif
      }
      transitToWaitForNTP();
    } else {
      delay(500);
    }
    break;
  case WAIT_FOR_NTP: {
    nixie.display(3, 3, 3, 3);
    ntpClient.update();
    if (ntpUpdateReceived(&ntpClient))
      transitToTickTock();
    delay(500);
    break;
  }
  case TICKTOCK: {
    seconds++;
    tempNow = millis();
    if (millis() - lastNtpUpdate > NTP_UPDATE_INTERVAL_MS) {
      lastNtpUpdate = tempNow;
      ntpClient.update();
    }
    if (extractTime(&ntpClient, &storedHours, &storedMinutes, &storedSeconds,
                    &tempHours, &tempMinutes, &seconds)) {
      hour1 = tempHours / 10;
      hour2 = tempHours % 10;
      minute1 = tempMinutes / 10;
      minute2 = tempMinutes % 10;
      nixie.display(hour1, hour2, minute1, minute2);
    } else if (seconds == 60) {
      seconds = 0;
      tickToNextMinute(&hour1, &hour2, &minute1, &minute2);
    }
    if (shouldMaintainNow) {
      transitToMaintenance();
    }
    delay(1000); // this is the heartbeat that ticks every second
    break;
  }
  case MAINTENANCE:
    nixie.cycle(MAINTENANCE_CYCLE_INTERVAL_MS);
    if (maintenanceCounter++ > MAINTENANCE_NUMBER_OF_CYCLES) {
      transitToTickTock();
    }
    break;
  }
}