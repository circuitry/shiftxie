#include "NixieEEPROM.hpp"
#include <Arduino.h>
#include <EEPROM.h>

// Starting address
#define ADDR_HAS_VALUE 0
#define ADDR_SSID 1
#define ADDR_PASSWORD 33

void writeHasValue(bool hasValue) {
  if (hasValue) {
    EEPROM.write(ADDR_HAS_VALUE, 1);
  } else {
    EEPROM.write(ADDR_HAS_VALUE, 0);
  }
  EEPROM.commit();
}

bool writeWiFiCredentials(String ssid, String password) {
  if (ssid.length() < 32 && password.length() < 32) {
    EEPROM.write(ADDR_HAS_VALUE, 1);
    const char *strSSID = ssid.c_str();
    const char *strPASSWORD = password.c_str();
    for (uint8_t i = 0; i <= ssid.length(); i++) {
      Serial.println(strSSID[i]);
      EEPROM.write(ADDR_SSID + i, strSSID[i]);
    }
    for (uint8_t i = 0; i <= password.length(); i++) {
      EEPROM.write(ADDR_PASSWORD + i, strPASSWORD[i]);
    }
    EEPROM.commit();
    delay(1000);
    return true;
  } else {
    return false;
  }
}

bool readWiFiCredentials(char *ssid, char *password) {
  if (EEPROM.read(ADDR_HAS_VALUE)) {
    uint8_t i = 0;
    char buffer;
    do {
      buffer = EEPROM.read(ADDR_SSID + i);
      ssid[i++] = buffer;
    } while (i < 32 && buffer != '\0');
    delay(200);
    i = 0;
    do {
      buffer = EEPROM.read(ADDR_PASSWORD + i);
      password[i++] = buffer;
    } while (i < 32 && buffer != '\0');
    delay(200);
    return true;
  } else {
    return false;
  }
}