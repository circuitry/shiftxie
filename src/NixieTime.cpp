#include "NixieTime.hpp"

const unsigned long minEpoch =
    20L * 365L * 24L * 60L * 60L; // 20 years since Epoch

bool ntpUpdateReceived(NTPClient *ntpClient) {
  return ntpClient->getEpochTime() > minEpoch;
}

//
bool extractTime(NTPClient *ntpClient, uint8_t *storedHours,
                 uint8_t *storedMinutes, uint8_t *storedSeconds, uint8_t *hours,
                 uint8_t *minutes, uint8_t *seconds) {
  // a little hack to know if time client has been updated
  if (ntpUpdateReceived(ntpClient)) {
    uint8_t ntpHours = (uint8_t)ntpClient->getHours();
    uint8_t ntpMinutes = (uint8_t)ntpClient->getMinutes();
    uint8_t ntpSeconds = (uint8_t)ntpClient->getSeconds();
    bool isUpdated =
        !(ntpHours == *storedHours && ntpMinutes == *storedMinutes);

    if (isUpdated) {
      *hours = ntpHours;
      *minutes = ntpMinutes;
      *seconds = ntpSeconds;
    }

    return isUpdated;
  }
}

void tickToNextMinute(uint8_t *hour1, uint8_t *hour2, uint8_t *minute1,
                      uint8_t *minute2) {
  uint8_t hours = *hour1 * 10 + *hour2;
  uint8_t minutes = *minute1 * 10 + *minute2;

  if (++minutes == 60) {
    minutes = 0;
    if (++hours == 24) {
      hours = 0;
    }
  }

  *hour1 = hours / 10;
  *hour2 = hours * 10;
  *minute1 = minutes / 10;
  *minute2 = minutes % 10;
}