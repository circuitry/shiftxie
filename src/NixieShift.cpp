#include "NixieShift.hpp"
#include <math.h>

uint8_t NixieShift::twoToThePowerOf(uint8_t n) {
  uint8_t sum = 1;
  for (int i = 0; i < n; i++) {
    sum *= 2;
  }
  return sum;
}

uint8_t NixieShift::arrayToUInt(uint8_t *bits) {
  uint8_t sum = 0;
  for (int i = 0; i < 8; i++) {
    sum += *(bits + i) * twoToThePowerOf(7 - i);
  }
  return sum;
}

void NixieShift::display(uint8_t n1, uint8_t n2, uint8_t n3, uint8_t n4) {
  uint8_t bits[40] = {0};
  bits[n1] = 1;
  bits[10 + n2] = 1;
  bits[20 + n3] = 1;
  bits[30 + n4] = 1;

  uint8_t s1 = arrayToUInt(bits);
  uint8_t s2 = arrayToUInt(bits + 8);
  uint8_t s3 = arrayToUInt(bits + 16);
  uint8_t s4 = arrayToUInt(bits + 24);
  uint8_t s5 = arrayToUInt(bits + 32);

  // LATCH and DATA signals are inverted as there's a
  // HEX Inverter used in the circuit (for powering the
  // bus with 5v)
  digitalWrite(_latchPin, HIGH);
  shiftOut(_dataPin, _clockPin, LSBFIRST, ~s5);
  shiftOut(_dataPin, _clockPin, LSBFIRST, ~s4);
  shiftOut(_dataPin, _clockPin, LSBFIRST, ~s3);
  shiftOut(_dataPin, _clockPin, LSBFIRST, ~s2);
  shiftOut(_dataPin, _clockPin, LSBFIRST, ~s1);
  digitalWrite(_latchPin, LOW);
}

void NixieShift::cycle(unsigned long interval) {
  for (uint8_t i = 0; i < 10; i++) {
    display(i, i, i, i);
    delay(interval);
  }
}