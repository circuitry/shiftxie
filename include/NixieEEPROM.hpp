#ifndef NIXIE_EEPROM_H
#define NIXIE_EEPROM_H
#include <Arduino.h>

// set or unset a byte in EEPROM that shows if there is
// any WiFi crendentials saved
void writeHasValue(bool hasValue);

// write the wifi credentials into the EEPROM
bool writeWiFiCredentials(String ssid, String password);

// allocate at least a length of 32) for both `ssid` and `password`
bool readWiFiCredentials(char *ssid, char *password);

#endif