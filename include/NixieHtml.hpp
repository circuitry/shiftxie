#ifndef NIXIE_HTML_H
#define NIXIE_HTML_H

const char homePage[] =
    "<html><head><style>body{background-image:linear-gradient(to right "
    "top,#0066ff,#0060e9,#0059d3,#0052bd,#0a4ba7);font-family:courier;display:"
    "flex;flex-direction:column;justify-content:center;align-items:center;"
    "width:100vw;color:#fffb}.title{font-weight:800;font-size:6rem;padding-"
    "bottom:4.5rem}input{color:black;margin:.3rem 0 1rem "
    "0;width:20rem;padding:.3rem}button{margin-top:4.5rem;width:100\%;padding:."
    "5rem;font-family:inherit;border:2px solid "
    "#fff9;background:none!important;color:#fff;font-size:1.5rem;border-radius:"
    "4px;cursor:pointer}</style><title>Nixie 9000</title></head><body><div "
    "class='title'>NIXIE 9000</div><form method='post' action='/wifi' "
    "accept-charset='utf-8' "
    "target='#'><div>SSID:</div><input type='text' "
    "name='SSID'><br><div>PASSWORD:</div><input type='text' "
    "name='PASSWORD'><div><button>Let's Get "
    "Started!</button></div></form></body></html>";

const char thankYouPage[] =
    "<html><head><style>body{background-image:linear-gradient(to right "
    "top,#0066ff,#0060e9,#0059d3,#0052bd,#0a4ba7);font-family:courier;display:"
    "flex;flex-direction:column;justify-content:center;align-items:center;"
    "width:100vw;color:#fffb}.title{font-weight:800;font-size:6rem;padding-"
    "bottom:4.5rem}</style><title>Nixie 9000</title></head><body><div "
    "class='title'>THANK YOU!!!</div></body></html>";

#endif