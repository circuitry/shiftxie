#ifndef NIXIE_TIME_H
#define NIXIE_TIME_H
#include <NTPClient.h>

// Whether NTP has received a time update from the NTP pool
bool ntpUpdateReceived(NTPClient *ntpClient);

// Extract the time we get from NTPClient and populate it into the passed
// in `time` pointer (SHOULD BE array of FOUR integers) so we get:
// [HOUR1, HOUR2, MINUTE1, MINUTE2], e.g., 17:45 -> [1,7,4,5].
//
// Will mutate `time` to [128,128,128,128] if we have not received an
// update from the NTPClient.
// void extractTime(NTPClient *ntpClient, uint8_t *time);
bool extractTime(NTPClient *ntpClient, uint8_t *storedHours,
                 uint8_t *storedMinutes, uint8_t *storedSeconds, uint8_t *hours,
                 uint8_t *minutes, uint8_t *seconds);

void tickToNextMinute(uint8_t *hour1, uint8_t *hour2, uint8_t *minute1,
                      uint8_t *minute2);

#endif