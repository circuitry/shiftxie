#ifndef NIXIE_SHIFT_H
#define NIXIE_SHIFT_H

#include <Arduino.h>
#include <stdint.h>

class NixieShift {
public:
  NixieShift(uint8_t latchPin, uint8_t clockPin, uint8_t dataPin)
      : _latchPin(latchPin), _clockPin(clockPin), _dataPin(dataPin) {
    pinMode(latchPin, OUTPUT);
    pinMode(clockPin, OUTPUT);
    pinMode(dataPin, OUTPUT);
  }

  // The digits for each nixies, please provide integer from 0 to 9.
  void display(uint8_t nixie1, uint8_t nixie2, uint8_t nixie3, uint8_t nixie4);

  // Cycle through all the digits in all four nixies in certain
  // interval (milliseconds)
  void cycle(unsigned long interval);

private:
  uint8_t _latchPin;
  uint8_t _clockPin;
  uint8_t _dataPin;
  uint8_t twoToThePowerOf(uint8_t n);
  uint8_t arrayToUInt(uint8_t *bits);
};

#endif